from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Gusti Ngurah Yama Adi Putra' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999,10,23) #TODO Implement this, format (Year, Month, Date)
npm = '1706979253' # TODO Implement this
kuliah = "University of Indonesia"
hobi = "Ngoding"
deskripsi = "Seorang mahasiswa yang berharap bisa lulus dengan predikat cumlaude"

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm,
    			'kuliah': kuliah, 'hobi': hobi, 'deskripsi': deskripsi}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
